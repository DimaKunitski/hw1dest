const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"],
      clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const newClient = [...new Set([ ...clients1, ...clients2 ])];
console.log(newClient);