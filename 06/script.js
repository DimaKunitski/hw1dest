const employee = {
    name: 'Vitalii',
    surname: 'Klichko'
}

const profile = {
    age: 40,
    salary: 100000
}

const boxerprofile = {...employee, ...profile}

console.log(boxerprofile);